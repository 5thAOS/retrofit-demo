package com.example.rany.retrofitdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.rany.retrofitdemo.api_service.ArticleService;
import com.example.rany.retrofitdemo.response.Article;
import com.example.rany.retrofitdemo.response.ArticleResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private Button btnGetArticle;
    private ListView lvArticle;
    private int page = 1;
    private int limit = 15;
    public static final String BASE_URL = "http://api-ams.me";
    public static final String TAG = "ooooo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnGetArticle = findViewById(R.id.btnGetArticle);
        lvArticle = findViewById(R.id.lvArticle);

        btnGetArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getArticles();
            }
        });

    }

    private void getArticles() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ArticleService service = retrofit.create(ArticleService.class);
        Call<ArticleResponse> call = service.getAllArticle(page, limit, "");
        call.enqueue(new Callback<ArticleResponse>() {
            @Override
            public void onResponse(Call<ArticleResponse> call, Response<ArticleResponse> response) {
//                Log.e(TAG, response.body().getMessage());
                List<Article> articles = response.body().getArticleList();

                // Create arraylist to store title of record
                ArrayList<String> titles = new ArrayList<>();
                for (Article article : articles){
                    titles.add(article.getTitle());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(
                    MainActivity.this,
                    android.R.layout.simple_list_item_1,
                        titles
                );
                lvArticle.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<ArticleResponse> call, Throwable t) {
                Log.e(TAG, "Error "+ t.getMessage());
            }
        });
    }

}
