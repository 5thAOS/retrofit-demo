package com.example.rany.retrofitdemo.api_service;

import com.example.rany.retrofitdemo.response.singup_login_response.UserResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface SingUpService {

    @Multipart
    @POST("/v1/api/users")
    Call<UserResponse> signUp(
            @Part("EMAIL") RequestBody email,
            @Part("NAME") RequestBody name,
            @Part("PASSWORD") RequestBody password,
            @Part("GENDER") RequestBody gender,
            @Part("TELEPHONE") RequestBody telephone,
            @Part("FACEBOOK_ID") RequestBody facebook_id,
            @Part MultipartBody.Part photo
            );

}
