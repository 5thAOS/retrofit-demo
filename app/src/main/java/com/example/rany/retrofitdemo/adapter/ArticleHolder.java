package com.example.rany.retrofitdemo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rany.retrofitdemo.R;
import com.example.rany.retrofitdemo.callback.OnRecyclerItemListener;
import com.example.rany.retrofitdemo.response.Article;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ArticleHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView tvTitle, tvDes, tvDate;
    ImageView imvArticle, imvDelete;
    OnRecyclerItemListener listener;

    public ArticleHolder(View itemView, OnRecyclerItemListener listener) {
        super(itemView);
        tvTitle = itemView.findViewById(R.id.tvTitle);
        tvDes = itemView.findViewById(R.id.tvDes);
        tvDate = itemView.findViewById(R.id.tvDate);
        imvArticle = itemView.findViewById(R.id.imageView);
        imvDelete = itemView.findViewById(R.id.imvDelete);
        this.listener = listener;

        imvDelete.setOnClickListener(this);

    }

    public String getDate(String date) {
        Date d = null;
        try {
            d = new SimpleDateFormat("yyyyMMddHHmmss").parse(date);
        }
        catch (Exception e)
        {}
        return String.valueOf(d);
    }

    public void onBind(Article article){
        tvTitle.setText(article.getTitle());
        tvDes.setText(article.getDescription());
        tvDate.setText(getDate(article.getCreatedDate()));
        Picasso.get()
                .load(article.getImage())
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_foreground)
                .into(imvArticle);


    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imvDelete:
                listener.onDeleteItem(getAdapterPosition());
                break;
        }
    }
}
