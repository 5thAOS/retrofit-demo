package com.example.rany.retrofitdemo.api_service;

import com.example.rany.retrofitdemo.response.singup_login_response.UserForm;
import com.example.rany.retrofitdemo.response.singup_login_response.UserResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginService {

    @POST("/v1/api/authentication")
    Call<UserResponse> userLogin(@Body UserForm userForm);

}
