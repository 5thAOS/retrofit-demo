package com.example.rany.retrofitdemo;

import android.app.SearchManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.rany.retrofitdemo.adapter.ArticleAdapter;
import com.example.rany.retrofitdemo.api_service.ArticleService;
import com.example.rany.retrofitdemo.callback.OnRecyclerItemListener;
import com.example.rany.retrofitdemo.response.Article;
import com.example.rany.retrofitdemo.response.ArticleResponse;
import com.example.rany.retrofitdemo.response.delete_response.ArticleDeleteRespone;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeScreen extends AppCompatActivity implements OnRecyclerItemListener {

    private RecyclerView rvArticle;
    public static final String BASE_URL = "http://api-ams.me";
    public static final String TAG = "ooooo";
    private ArticleAdapter adapter;
    private int page = 1;
    private Retrofit retrofit;
    private ArticleService service;
    private int total_page = 0;

    private int total_item = 0;
    private int last_item = 0;
    public static final int threstHold = 2;
    public  int limit = 10;
    private SearchView searchView;
    private String search = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        setUpRetrofit();
        setUpRecyclerView();

        final LinearLayoutManager layoutManager = (LinearLayoutManager) rvArticle.getLayoutManager();
        rvArticle.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                total_item = layoutManager.getItemCount();
                last_item = layoutManager.findLastCompletelyVisibleItemPosition();
                Log.e(TAG, "onScrolled: "+ total_page + ", "+ total_item );
                if(dy < 0){
                    if(total_item <= last_item + threstHold){
                        if(page > total_page){
                            return;
                        }
                        page++;
                        getArticles(page, limit, search);
                    }
                }
            }
        });

    }

    private void setUpRecyclerView(){
        rvArticle = findViewById(R.id.rcArticle);
        rvArticle.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ArticleAdapter();
        rvArticle.setAdapter(adapter);
        adapter.setRecyclerListerner(this);
        getArticles(page, limit, search);
    }

    private void setUpRetrofit(){
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(ArticleService.class);
    }

    private void getArticles(int pages, int limit, String s) {
        Call<ArticleResponse> call = service.getAllArticle(pages, limit, s);
        call.enqueue(new Callback<ArticleResponse>() {
            @Override
            public void onResponse(Call<ArticleResponse> call, Response<ArticleResponse> response) {
                List<Article> articleResponseList = response.body().getArticleList();
                adapter.getArticles(articleResponseList);
                // Get total page from server
                total_page = response.body().getPagination().getTotalPages();
            }

            @Override
            public void onFailure(Call<ArticleResponse> call, Throwable t) {
                Log.e(TAG, "Error "+ t.getMessage());
            }
        });
    }


    @Override
    public void onDeleteItem(final int position) {
        Article article = adapter.getItemArticl(position);
        Call<ArticleDeleteRespone> call = service.deleteArticle(position);
        call.enqueue(new Callback<ArticleDeleteRespone>() {
            @Override
            public void onResponse(Call<ArticleDeleteRespone> call, Response<ArticleDeleteRespone> response) {
                if(response.isSuccessful()){
                    ArticleDeleteRespone articledelete = response.body();
                    if(articledelete.getArticledelete() != null){
                        adapter.deleteArticle(position);
                        Log.e(TAG, "onResponse: SUccess" );
                    }
                }
            }

            @Override
            public void onFailure(Call<ArticleDeleteRespone> call, Throwable t) {
                Log.e(TAG, "onFailure: "+ t.getMessage() );
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item, menu);
        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.m_search));
        SearchManager manager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.m_search:
                onSearchClick();
        }
        return super.onOptionsItemSelected(item);
    }

    private void onSearchClick(){
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.removeData();
                search = query;
                getArticles(page, limit, search);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.e(TAG, "onQueryTextChange: "+ newText );
                adapter.removeData();
                search = newText;
                getArticles(page, limit, search);
                return false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                adapter.removeData();
                getArticles(page, limit, "");
                return false;
            }
        });
    }


}
