package com.example.rany.retrofitdemo;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.rany.retrofitdemo.api_service.ServiceGenerator;
import com.example.rany.retrofitdemo.api_service.SingUpService;
import com.example.rany.retrofitdemo.response.singup_login_response.UserResponse;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpScreen extends AppCompatActivity {

    private static final int OPEN_GALLERY = 1;
    private static final int ACCEPT_PERMISSION = 1;
    public static final String TAG = "ooooo";
    private Button browse, singup;
    private EditText edName, edEmail, edPsw;
    private ImageView imgView;
    private String imagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_screen);

        initView();
        initEvent();
    }

    private void initEvent() {
        browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ContextCompat.checkSelfPermission(SignUpScreen.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) !=
                        getPackageManager().PERMISSION_GRANTED){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{
                                Manifest.permission.READ_EXTERNAL_STORAGE},
                                ACCEPT_PERMISSION);
                    }
                }
                else
                    openGallery();
            }
        });

        singup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // convert to requestbody
                RequestBody email = createRequestBody(edEmail.getText().toString());
                RequestBody name = createRequestBody(edName.getText().toString());
                RequestBody psw = createRequestBody(edPsw.getText().toString());
                RequestBody gender = createRequestBody("f");
                RequestBody phone = createRequestBody("0987654213");
                RequestBody fbId = createRequestBody("12345678");
                RequestBody requestBody = RequestBody.create(
                        MediaType.parse("multipart/form-data"), new File(imagePath));

                MultipartBody.Part photo = MultipartBody.Part.createFormData(
                        "PHOTO", new File(imagePath).getName(), requestBody);

                SingUpService service = ServiceGenerator.createService(SingUpService.class);
                service.signUp(
                        email,
                        name,
                        psw,
                        gender,
                        phone,
                        fbId,
                        photo
                ).enqueue(new Callback<UserResponse>() {
                    @Override
                    public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                        if(response.isSuccessful()){
                            if(response.body().getUsersignup() != null){
                                startActivity(new Intent(SignUpScreen.this, LoginScreen.class));
                                finish();
                            }
                        }
                        else Toast.makeText(SignUpScreen.this, "Failed! ", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<UserResponse> call, Throwable t) {
                        Log.e(TAG, "onFailure: "+ t.getMessage() );
                    }
                });
            }
        });
    }

    private RequestBody createRequestBody(String value){
        return RequestBody.create(MediaType.parse("text/plain"), value);

    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                Uri.parse(MediaStore.Images.Media.DATA));
        intent.setType("image/*");
        startActivityForResult(intent, OPEN_GALLERY);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == ACCEPT_PERMISSION){
            if(grantResults[0] == getPackageManager().PERMISSION_GRANTED){
                openGallery();
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == OPEN_GALLERY){
            if(resultCode == RESULT_OK){
                Uri uri = data.getData();
                imgView.setImageURI(uri);
                String[] column = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(uri, 
                        column, null, null, null);
                cursor.moveToFirst();
                imagePath = cursor.getString(cursor.getColumnIndex(column[0]));
                }
        }

    }
    private void initView() {
        browse = findViewById(R.id.btnBrowse);
        singup = findViewById(R.id.btnSingup);
        edEmail = findViewById(R.id.edEmail);
        edName = findViewById(R.id.edName);
        edPsw = findViewById(R.id.edPsw);
        imgView = findViewById(R.id.imvView);
    }
}
