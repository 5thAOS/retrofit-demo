package com.example.rany.retrofitdemo.api_service;


import com.example.rany.retrofitdemo.response.ArticleResponse;
import com.example.rany.retrofitdemo.response.delete_response.ArticleDeleteRespone;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ArticleService {

    @GET("/v1/api/articles")
    Call<ArticleResponse> getAllArticle(
            @Query("page") int page,
            @Query("limit") int limit,
            @Query("title") String title
    );

    @DELETE("/v1/api/articles/{id}")
    Call<ArticleDeleteRespone> deleteArticle(@Path("id") int id);

}
