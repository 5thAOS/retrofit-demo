package com.example.rany.retrofitdemo.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rany.retrofitdemo.R;
import com.example.rany.retrofitdemo.callback.OnRecyclerItemListener;
import com.example.rany.retrofitdemo.response.Article;
import java.util.ArrayList;
import java.util.List;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleHolder> {

    List<Article> articleList;
    OnRecyclerItemListener listener;

    public ArticleAdapter(){
        articleList = new ArrayList<>();
    }

    public void setRecyclerListerner(OnRecyclerItemListener listener){
        this.listener = listener;
    }

    public Article getItemArticl(int position){
        return this.articleList.get(position);
    }

    public void deleteArticle(int position){
        articleList.remove(position);
        notifyItemChanged(position);
    }

    public void removeData(){
        this.articleList.clear();
    }

    @NonNull
    @Override
    public ArticleHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
            R.layout.article_item, parent, false
        );
        return new ArticleHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleHolder holder, int position) {
        Article article = articleList.get(position);
        holder.onBind(article);
    }

    public void getArticles(List<Article> articles){
        articleList.addAll(articles);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }
}
