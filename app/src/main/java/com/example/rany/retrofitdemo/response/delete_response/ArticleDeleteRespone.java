package com.example.rany.retrofitdemo.response.delete_response;

import com.google.gson.annotations.SerializedName;

public class ArticleDeleteRespone {
    @SerializedName("CODE")
    private String code;
    @SerializedName("MESSAGE")
    private String message;
    @SerializedName("DATA")
    private Articledelete articledelete;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Articledelete getArticledelete() {
        return articledelete;
    }

    public void setArticledelete(Articledelete articledelete) {
        this.articledelete = articledelete;
    }
}
